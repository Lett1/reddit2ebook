import praw
from jinja2 import Environment, FileSystemLoader
import sys

#Helper class for jinja2 templating, holds all the variables that get rendered to html
class html_comment:
    def __init__(self,author,body,ups,downs,reply_author=None,reply_body=None):
        self.author = author
        self.body = body
        self.ups = ups
        self.downs = downs
        self.reply_author = reply_author
        self.reply_body = reply_body

def main(user,password,url):
    #Comments for the html are stored here
    submission_comments = []

    #login to reddit duh
    r = praw.Reddit('Reddit2Kindle 0.1 by u/Lett1')
    r.login(user,password)

    #get the submission
    submission = r.get_submission(url)
    comments = submission.comments

    del comments[-1]
    comments.sort(key=lambda x: x.score,reverse=True)

    for comment in submission.comments:
        filtered_replies = filter (lambda a: type(a) != praw.objects.MoreComments, comment.replies)
        filtered_replies.sort(key=lambda x: x.score,reverse=True)

        if filtered_replies:
            submission_comments.append(html_comment(comment.author.name,comment.body,comment.ups,comment.downs,
                reply_author=filtered_replies[0].author.name,reply_body=filtered_replies[0].body))
        else:
            submission_comments.append(html_comment(comment.author.name,comment.body,comment.ups,comment.downs))
        

    templateLoader = FileSystemLoader( searchpath="./" )
    templateEnv = Environment( loader=templateLoader )
    TEMPLATE_FILE = "template.html"
    template = templateEnv.get_template( TEMPLATE_FILE )

    # Specify any input variables to the template as a dictionary.
    templateVars = { "submission" : submission,
                     "submission_comments" : submission_comments }

    # Finally, process the template to produce our final text.
    outputText = template.render( templateVars )

    with open("redditbook.html", "wb") as fh:
        fh.write(outputText.encode("utf-8"))

if __name__ == "__main__":
    print len(sys.argv)
    if len(sys.argv)-1 < 3:
        sys.stderr.write('Usage: redditcommentscraper USERNAME PASSWORD THREAD_URL ')
        sys.exit(1)
    else:
        main(sys.argv[1],sys.argv[2],sys.argv[3])